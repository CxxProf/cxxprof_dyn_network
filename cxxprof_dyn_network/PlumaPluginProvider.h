
#pragma once

#include <Pluma/Pluma.hpp>

#include <cxxprof_preloader/IDynCxxProf.h>
#include <cxxprof_preloader/IDynCxxProfProvider.h>

#include "cxxprof_dyn_network/NetworkCxxProf.h"

namespace CxxProf
{

    PLUMA_INHERIT_PROVIDER(NetworkCxxProf, IDynCxxProf);

}
